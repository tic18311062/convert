package com.osuna.jorge.converter;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.osuna.jorge.converter.databinding.ActivityMainBinding;

public class MainActivity extends Activity {

    private TextView mTextView;
    private ActivityMainBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());
    }

    public void convert (View view){
        TextView txKm = (TextView) findViewById(R.id.tv_km);
        EditText edMile = (EditText) findViewById(R.id.ed_mile);
        double km ;
        km = 1.609 * Integer.parseInt(edMile.getText().toString());
        txKm.setText(Double.toString(km));
    }
}